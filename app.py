from flask import Flask, send_file
import requests
from io import BytesIO
from flask_caching import Cache
from fastapi.responses import StreamingResponse

cache = Cache(config={'CACHE_TYPE': 'SimpleCache'})
app = Flask(__name__)
cache.init_app(app)



@app.route('/cache')
@cache.cached(timeout=10)
def hello():
    f = open("thaivd.jpg", "rb")
    filedata = f.read()
    mimetype_by_name = "image/jpeg"
    resp = send_file(BytesIO(filedata), mimetype=mimetype_by_name)
    print(type(resp))
    return resp

def abc():
    return 0




if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
